
from bs4 import BeautifulSoup
import requests

# just to mask it a bit
headers = {'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36'}

print("Started loading website data.")

pageHidmet = requests.get("http://www.hidmet.gov.rs/latin/prognoza/index.php", headers=headers)
soup1 = BeautifulSoup(pageHidmet.content, 'html.parser')

pageAccuW = requests.get("https://www.accuweather.com/sr/rs/novi-sad/298486/daily-weather-forecast/298486", headers=headers)
soup2 = BeautifulSoup(pageAccuW.content, 'html.parser')

pageUmbrella = requests.get("https://www.weather2umbrella.com/vremenska-prognoza-novi-sad-serbia-sr/7-dana", headers=headers)
soup3 = BeautifulSoup(pageUmbrella.content, 'html.parser')

print("Finished loading website data.")


#   ***umbrella***
wrapper = soup3.find("a", {"data-id" : "1"}) # different way to search by specific attr
minTemp1 = int(wrapper.find("p", class_ = "day_min").text.strip()[:-1])
maxTemp1 = int(wrapper.find("p", class_ = "day_max").text.strip()[:-1])


#   ***accuweather***
wrapper = soup2.find_all("div", class_ = "daily-wrapper")[1]
minTemp2 = int(wrapper.find("span", class_ = "low").text[1:-1]) #string slicing the degree symbol and "/"
maxTemp2 = int(wrapper.find("span", class_ = "high").text[:-1])


#   ***hidmet***
red = soup1.find_all("tr")[4]  # ovo je red za NS
kolone = red.find_all("td")
maxTemp3 = int(kolone[4].text)
minTemp3 = int(kolone[3].text)


minTempAll = (minTemp1 + minTemp3 + minTemp3)/3
maxTempAll = (maxTemp1 + maxTemp3 + maxTemp3)/3


print("Max temp for Novi Sad will be "+str(round(maxTempAll)))
print("Min temp for Novi Sad will be "+str(round(minTempAll)))

